variable "node_count" {
  default = "3"
}

resource "google_compute_instance" "nodes" {
  count        = var.node_count
  name         = "node${count.index + 1}"
  machine_type = "e2-small"
  zone         = "asia-east1-a"
  boot_disk {
    initialize_params {
      size  = "20"
      image = "ubuntu-2004-focal-v20211212"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }

  metadata = {
    ssh-keys = "root:${file("~/.ssh/id_rsa.pub")}"
  }
}
