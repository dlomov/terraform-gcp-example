# Примеры работы с GCP с помощью Terraform
## 1. Создать 3 вм инстанса на GCP. Условие — не юзать для имён нод явный список или словарь.
- Размер диска 20 Гб
- Тип e2-small
- Регион на выбор
- Имя node 1 node 2 node 3 (не с 0)
---

<details>
<summary>Смотреть код</summary>

Выносим настройки провайдера в файл provider.tf.
Если не использовать GCPCLI, то креденшнл можно прописать так:
credentials = file("mygcp-creds.json") 

### provider.tf
```hcl2
provider "google" {
  project     = "black-skyline-352214"
  region      = "asia-east1"
  zone        = "asia-east1-a"
}
```
### task.tf
```hcl2

variable "node_count" {
  default = "3"
}

resource "google_compute_instance" "nodes" {
  count        = 3
  #именуем ноды через count.index+1
  name         = "node${count.index + 1}"
  machine_type = "e2-small"
  zone         = "asia-east1-a"
  boot_disk {
    initialize_params {
      size  = "20"
      image = "ubuntu-2004-focal-v20211212"
    }
  }

  network_interface {
	#это сетка по дефолту
    network = "default"
    access_config {
    }
  }

  metadata = {
	#кидаем сразу свой ssh-keygen -t rsa -C "you@mail.com"
    ssh-keys = "root:${file("~/.ssh/id_rsa.pub")}"
  }
}
```
### output.tf
```hcl2
output "nodes_internal_ip" {
  value = google_compute_instance.nodes[*].network_interface.0.network_ip
}

output "nodes_external_ip" {
  value = google_compute_instance.nodes[*].network_interface.0.access_config.0.nat_ip
}
```
Вывод внешних и внутренних ip ресурсов в консоле:
```
Outputs:

nodes_external_ip = [
  "35.201.150.175",
  "34.80.202.162",
  "34.80.71.249",
]
nodes_internal_ip = [
  "10.140.0.12",
  "10.140.0.14",
  "10.140.0.13",
]
```
Проверим с помощью gcloud
```
$ gcloud compute instances list
NAME   ZONE          MACHINE_TYPE  PREEMPTIBLE  INTERNAL_IP  EXTERNAL_IP     STATUS
node1  asia-east1-a  e2-small                   10.140.0.12  35.201.150.175  RUNNING
node2  asia-east1-a  e2-small                   10.140.0.14  34.80.202.162   RUNNING
node3  asia-east1-a  e2-small                   10.140.0.13  34.80.71.249    RUNNING
```

</details>

---

## 2. Работа с provisioner 
1. Дополнить манифест с первого задания
- Провижен local-exec должен записать в файл host.list host_name nat_ip все 3 хоста.
2. Провижен remote-exec
- Инсталл nginx
- Наполнить index.html - Juneway host_ip host_name
---

<details>
<summary>Смотреть код</summary>




### task.tf

```hcl2
variable "private_key_path" {
  description = "priv key"
}

variable "public_key_path" {
  description = "pub key"
}

variable "node_count" {
  default = "3"
}

resource "google_compute_instance" "vm_instance" {
  count        = var.node_count
  name         = "node-${count.index + 1}"
  machine_type = "e2-small"
  connection {
    host        = self.network_interface.0.access_config.0.nat_ip
    type        = "ssh"
    user        = "root"
    private_key = file(var.private_key_path)
    timeout     = "2m"
  }

  metadata = {
    ssh-keys = "root:${file(var.public_key_path)}"
  }

  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} > host.list"
  }

  provisioner "file" {
    source      = "scripts/nginx.sh"
    destination = "/tmp/nginx.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} > /tmp/host.list",
      "cd /tmp",
      "chmod +x /tmp/nginx.sh",
      "/tmp/nginx.sh args"
    ]
  }

  boot_disk {
    initialize_params {
      size  = "20"
      image = "ubuntu-2004-focal-v20211212"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}

```
### terraform.tfvars
```hcl2
private_key_path = "~/.ssh/id_rsa"
public_key_path  = "~/.ssh/id_rsa.pub"
```

### firewall.tf
```hcl2
resource "google_compute_firewall" "http" {
  name    = "http-for-nginx"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  source_ranges = ["0.0.0.0/0"]

}
```
### scripts/nginx.sh
``` bash
#!/bin/bash
apt -y update
apt -y install nginx
myip=$(cat /tmp/host.list)
cat <<EOF >/var/www/html/index.nginx-debian.html
<!DOCTYPE html>
<html>
<head>
<title>Welcome to Juneway!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to Juneway!</h1>
<h2>$myip</h2>
<p><em>Thank you for using 228.</em></p>
</body>
</html>
EOF
service nginx start
```

</details>

---
## 3. Работа с Firewall
1. Добавить в манифест
- output список имён Хостов
- айди проекта

2. Создать в фаеволе правило разрешающее
- tcp 80 - all
- tcp 433 - all
- udp 10000-20000 - 10.0.0.23 (только для одного хоста)

<details>
<summary>Смотреть код</summary>

### firewall.tf

```hcl2
resource "google_compute_firewall" "http" {
  name    = "http-for-nginx"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }
  source_ranges = ["0.0.0.0/0"]

}
resource "google_compute_firewall" "udp" {
  name    = "udp-for-nginx"
  network = "default"
  allow {
    protocol = "udp"
    ports    = ["10000-20000"]
  }
  source_ranges = ["10.0.0.23"]
}
```

### output.tf

```hcl2
output "vm_instance_internal_ip" {
  value = google_compute_instance.vm_instance[*].network_interface.0.network_ip
}

output "vm_instance_external_ip" {
  value = google_compute_instance.vm_instance[*].network_interface.0.access_config.0.nat_ip
}
output "hostname" {
  value = google_compute_instance.vm_instance[*].name
}
output "id" {
  value = google_compute_instance.vm_instance[*].id
}


```

</details>

---