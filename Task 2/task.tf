variable "private_key_path" {
  description = "priv key"
}

variable "public_key_path" {
  description = "pub key"
}

variable "node_count" {
  default = "3"
}

resource "google_compute_instance" "vm_instance" {
  count        = var.node_count
  name         = "node-${count.index + 1}"
  machine_type = "e2-small"
  connection {
    host        = self.network_interface.0.access_config.0.nat_ip
    type        = "ssh"
    user        = "root"
    private_key = file(var.private_key_path)
    timeout     = "2m"
  }

  metadata = {
    ssh-keys = "root:${file(var.public_key_path)}"
  }

  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} >> host.list"
  }

  provisioner "file" {
    source      = "scripts/nginx.sh"
    destination = "/tmp/nginx.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} >> /tmp/host.list",
      "cd /tmp",
      "chmod +x /tmp/nginx.sh",
      "/tmp/nginx.sh args"
    ]
  }

  boot_disk {
    initialize_params {
      size  = "20"
      image = "ubuntu-2004-focal-v20211212"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}

