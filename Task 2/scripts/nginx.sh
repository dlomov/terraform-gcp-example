#!/bin/bash
apt -y update
apt -y install nginx
myip=$(cat /tmp/host.list)
cat <<EOF >/var/www/html/index.nginx-debian.html
<!DOCTYPE html>
<html>
<head>
<title>Welcome to Juneway!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to Juneway!</h1>
<h2>$myip</h2>
<p><em>Thank you for using 228.</em></p>
</body>
</html>
EOF
service nginx start