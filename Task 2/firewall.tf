resource "google_compute_firewall" "http" {
  name    = "http-for-nginx"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }
  source_ranges = ["0.0.0.0/0"]

}
resource "google_compute_firewall" "udp" {
  name    = "udp-for-nginx"
  network = "default"
  allow {
    protocol = "udp"
    ports    = ["10000-20000"]
  }
  source_ranges = ["10.0.0.23"]
}
